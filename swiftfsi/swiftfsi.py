#!/usr/bin/env python3

# This file is Copyright (c) 2018-2019 Florent Kermarrec <florent@enjoy-digital.fr>
# This file is Copyright (c) 2018-2019 David Shah <dave@ds0.me>
# This file is Copyright (c) 2020-2023 Raptor Engineering, LLC
# License: BSD

import os
import argparse
import subprocess
import tempfile

from migen import *

from litex import get_data_mod

from litex.soc.interconnect import wishbone, stream
from litex.soc.interconnect.csr import *
from litex.gen.common import reverse_bytes
from litex.build.io import SDRTristate

kB = 1024
mB = 1024*kB

# Swift OpenFSI interface --------------------------------------------------------------------------

class OpenFSIMaster(Module, AutoCSR):
    def __init__(self, platform, pads, clk_freq, endianness="big", debug_pads=None):
        self.slave_bus    = slave_bus    = wishbone.Interface(data_width=32, adr_width=30)

        # Bus endianness handlers
        self.slave_dat_w = Signal(32)
        self.slave_dat_r = Signal(32)
        self.comb += self.slave_dat_w.eq(slave_bus.dat_w if endianness == "big" else reverse_bytes(slave_bus.dat_w))
        self.comb += slave_bus.dat_r.eq(self.slave_dat_r if endianness == "big" else reverse_bytes(self.slave_dat_r))

        # FSI bus signals
        self.fsi_data_in = Signal()
        self.fsi_data_out = Signal()
        self.fsi_data_direction = Signal()
        self.fsi_clock = Signal()

        self.specials += Instance("fsi_master_litex_shim",
            # Configuration data
            i_sys_clk_freq = clk_freq,

            # Wishbone slave port signals
            i_slave_wb_cyc = slave_bus.cyc,
            i_slave_wb_stb = slave_bus.stb,
            i_slave_wb_we = slave_bus.we,
            i_slave_wb_addr = slave_bus.adr,
            i_slave_wb_dat_w = self.slave_dat_w,
            o_slave_wb_dat_r = self.slave_dat_r,
            i_slave_wb_sel = slave_bus.sel,
            o_slave_wb_ack = slave_bus.ack,
            o_slave_wb_err = slave_bus.err,

            # Clock and reset
            # Put the peripheral on the both main system clock and reset domains
            i_peripheral_reset = ResetSignal('sys'),
            i_peripheral_clock = ClockSignal('sys'),

            # FSI interface
            i_fsi_data_in = self.fsi_data_in,
            o_fsi_data_out = self.fsi_data_out,
            o_fsi_data_direction = self.fsi_data_direction,
            o_fsi_clock_out = self.fsi_clock
        )
        # Add Verilog source files
        self.add_sources(platform)

        self.fsi_data_tristate = Signal()
        self.comb += self.fsi_data_tristate.eq(~self.fsi_data_direction)
        self.specials += Instance("BB",
            io_B = pads.data,
            i_I = self.fsi_data_out,
            i_T = self.fsi_data_tristate,
            o_O = self.fsi_data_in
        )
        self.comb += pads.data_direction.eq(self.fsi_data_direction)
        self.comb += pads.clock.eq(self.fsi_clock)

        # Debug interface
        if (debug_pads is not None):
            self.comb += debug_pads.led_15.eq(self.fsi_clock)
            self.comb += debug_pads.led_14.eq(self.fsi_data_in)
            self.comb += debug_pads.led_13.eq(self.fsi_data_direction)


    @staticmethod
    def add_sources(platform):
        vdir = get_data_mod("peripheral", "swiftfsi").data_location
        platform.add_source(os.path.join(vdir, "wishbone_interface.v"))
        platform.add_source(os.path.join(vdir, "fsi_master.v"))
